# NIC Stats
### Instalación con docker
Levantamos los containers:
```sh
docker-compose up -d
```
Corremos los DDL y DML en alguna consola de postgres (por defecto localhost:5432), están acá: 
```sh
ddl/ddl.sql
ddl/dml.sql
```
Ver el log de huey en tiempo real
```sh
docker-compose logs -f huey_scrapers huey_prechecks
```
Crear directorio de logs
```sh
mkdir scraping/logs
```
Correr tareas
```sh
docker exec nic_stats_huey_1 python run.py --limit 10 -T
```
Donde "nic_stats_huey_1" es el nombre del container (puede cambiar), --limit es la cantidad de dominios a correr y -T es un flag que si está presente trunca la tabla de metricas antes de correr

Para reiniciar huey y que también se borren las tareas
```sh
docker-compose stop redis huey_scrapers huey_prechecks; docker-compose rm huey_scrapers huey_prechecks redis; docker-compose up huey_scrapers huey_prechecks redis
```
Para reiniciar huey y que también se borren las tareas y además limitar el CPU hay que correrlo con --compatibility
```sh
docker-compose stop redis huey_scrapers huey_prechecks;docker-compose rm huey_scrapers huey_prechecks redis; docker-compose --compatibility up huey_scrapers huey_prechecks redis
```
Borrar logs
```sh
sudo rm scraping/*error.log*
```
### Instalación sin docker

Instalar python 3.8 y venv

```sh
$ sudo apt-get install python3.8
$ sudo apt-get install python3.8-venv
$ sudo apt-get install chromium-chromedriver
```
Creamos el virtual env
```sh
$ python3.8 -m venv ~/.virtualenvs/nic_stats
```
Clonamos el proyecto
```sh
$ git clone https://gitlab.com/gaia-software/nic-stats.git
```
Activamos el virtualenv
```sh
$ source ~/.virtualenvs/nic_stats/bin/activate
```
Instalamos dependencias
```sh
$ pip install -r requirements.txt
```

Levantamos los containers (es necesario para el postgres):
```sh
docker-compose up -d
```
Corremos los DDLs y DMLS en 
```sh
ddl/ddl.sql
ddl/dml.sql
```
Para correr los scripts fuera de docker debemos agregar las variables de entorno correspondientes, por ejemplo:
```sh
TS_DBNAME=nic_stats TS_USER=postgres TS_PASSWORD=postgres TS_HOST=localhost TS_PORT=5436 REDIS_HOST=localhost REDIS_PORT=6379 huey_consumer.py worker.scrapers_tasks.huey -k greenlet -w 64
```

## Filler
### Insert de dominios desde .csv
Tenemos que correr esto:
```sh
$ python csv_filler.py -f "/path/to/20210616_allDomains.csv"
```
Podemos modificar los parametros de la DB agregando variables de entorno, tipo:
```sh
$ POSTGRES_HOST=postgres python csv_filler.py -f "/path/to/20210616_allDomains.csv"
```
## Testing
Para correr los tests tenemos que pararnos en la scraping y ejecutar
```sh
python -m tests
```

## Metabase
Al inicio el container de metabase fallará porque todavía no está la base de datos cargada. Por eso con el container de postgres_metabase levantado corremos:
```sh
docker exec -i nic_stats_postgres_metabase_1 psql -U postgres < metabase/metabase.sql
```
Esto creara la base de datos y cargará el dump en dicho postgres. Luego sí podemos levantar el container de metabase:
```sh
docker-compose up -d metabase 
```
Si queremos hacer un dump de nuestra base de metabase hacemos
```sh
docker exec nic_stats_postgres_metabase_1 pg_dump -h localhost -p 5432 -U postgres -d metabase -C > metabase/metabase.sql
```

---

## Agregar un Scraper nuevo al circuito

1. Agregar un nuevo scraper en:

>    scraping > scrapers > nombrescraper_scraper.py

2. Nombrar la clase como:  NombreScraperScraper
3. Debe heredar de la clase Scraper
4. En su \__init\__ debe contener como variables de clase, las columnas en las que escribirá en la bbdd, además de llamar al init de su padre
5. Dentro de la clase, debe contener un array donde defina qué prechecks le corresponden, como por ej:

         prechecks = [DNSResolutionPrecheck, HTTPConnectedPrecheck]
 
6. Agregar las métricas que va a escirbir (columnas de la bbdd) en el archivo ddl.sql. 
7. Configurar un logger de la siguiente manera:
    *  Agregar una config de logger en el archivo logger/config.py
    *  configurar también el archivo logger/loggers.py   
    *  Dentro del Scraper nuevo, agregar una función llamada get_logger que retorne el logger configurado del punto anterior

8. Realizar un método dentro del Scraper llamado "check" el cual realizará todo lo necesario para el correcto funcionamiento del Scraper, junto con su escritura en la bbdd en las columnas self.metrics['nombre_columna'] segun corresponda.
9. Agregar en worker/scraper/tasks.py la tarea de huey de la siguiente manera:

```
@huey.task()
def run_nombre_scraper(*args, **runkwargs):
    run_scraper(SSLScraper, *args, **runkwargs)
```

10. Agregar en > scraping/worker/\__init\__.py nuestro nuevo Scraper de la siguiente manera:

```    
'nuevoscraper': {
        'class':NuevoScraper,
        'run_fn': run_nuevo_scraper,
    },
```

11. Agregar nuestro nuevo Scraper al runer de la siguiente manera:

> scraping/run.py

```    
rn = Runner(['responsiveness', 'accessibility', 'nuevoscraper'], DatabaseManager())
```

## Kubernetes
Si se quiere probar de forma local habrá que instalar primero minikube.
Luego instalar kubectl. Recomiendo instalar bash-completion también

Para usar una imagen local (al menos con minikube) debemos commitear nuestra imagen de docker de Python usando el Dockerfile.prod y correr lo siguiente:
```
eval $(minikube docker-env)    
```
```
docker image build . -f Dockerfile.prod -t "python-frida:latest" -t "python-frida:prod"
```
Sumado a lo anterior en el manifiesto tendremos que agregar la cláusula: "imagePullPolicy: Never" para que la imagen sea levantada del propio nodo en vez del dockerhub en Internet.

### Comandos utiles
Ver los pods levantados
```
kubectl get pods -o wide   
```
Aplicar manifiesto
```
kubectl apply -f timescale.yaml   
```
Borrar cosas declaradas en un manifiesto
```
kubectl delete -f timescale.yaml   
```
Borrar un algo en particular es "kubectl delete x nombre" donde x es el tipo de objeto a borrar, por ejemplo:
```
kubectl delete pod metabase-767b6f5b99-2gs4l   
kubectl delete service metabase   
kubectl delete deployment metabase   
```
Describe nos da información detallada de algo, por ejemplo:
```
kubectl describe service redis  
```
Para acceder un servicio directamente en nuestra máquina podemos hacer un port-forward:
```
kubectl port-forward timescale-db-0 5437:5432
```
Para ejecutar comandos en un pod:
```
kubectl exec -ti nombre-pod -- comando
```
Para ver logs de un pod:
```
kubectl logs -f huey-scrapers-85864d6b5f-lskbr
```
### Instrucciones para deployar
0. Creamos el namespace
```
kubectl apply -f 00-frida_namespace.yaml
```
1. Primero debemos crear un archivo llamado secret.yaml y que tenga la siguiente info:
```
apiVersion: v1
kind: Secret
metadata:
  name: frida-secret
  namespace: frida
type: Opaque
stringData:
  timescalePassword: ******
  metabaseDBPassword: ********
  domainsDBName: ********
  domainsUser: ********
  domainsPassword: ********
  domainsHost: ********
  domainsPort: "5432"
  domainsTableName: ********
```
Los parametros que comienzan con domains, hacen referencia a la base de datos de dominios que se utilizará para mantener actualizada esta base.
Como requisito debe ser un postgres con una tabla monocolumnar cuyo nombre de columna sea domain_name
1b. También debemos crear un secret para acceder a las credenciales del repo privado de imagenes de docker, de esta manera:
```
kubectl create secret -n=frida docker-registry regcred --docker-server=registry-ext.boletinoficial.gob.ar --docker-username=*** --docker-password=***
```
2. Una vez completado con las credenciales, corremos:
```
kubectl apply -f secret.yaml
```
3. Luego aplicamos el manifiesto de timescale. Pero antes necesitamos crear el
   pvc que se utiliza para backups con:
```
kubectl apply -f backup_pvc.yaml
kubectl apply -f 01-timescale.yaml
```
4. Una vez levantado podremos accederlo haciendo:
```
kubectl port-forward timescale-db-0 5437:5432
```
5. Accedemos con alguna consola de SQL y corremos los scripts DDL Y DML (en el directorio "ddl") para poder crear la base de datos. Es **importante** primero crear la base de datos y habilitar la extensión de postgres (está la instrucción en el archivo). Una vez realizado estos pasos nos conectamos a la base recién creada y ejecutamos el resto de las sentencias


6. Levantamos los pods de los scrapers en este orden:
```
kubectl apply -f 02-redis.yaml
kubectl apply -f 03-huey_prechecks.yaml
kubectl apply -f 04-huey_scrapers.yaml
```


7. Necesitamos correr el script que llena de dominios la base de datos, para eso primero, copiamos nuestro .csv con dominios:
```
kubectl cp /path/to/20210616_allDomains.csv huey-scrapers-7b4c59c696-rqcfd:/tmp/domains.csv
```
Luego ejecutamos el script que los carga:
```
kubectl exec -ti huey-scrapers-7b4c59c696-rqcfd -- python /app/domains/csv_filler.py -f "/tmp/domains.csv"
```
8. Procedemos a levantar el postgres de metabase:
```
kubectl apply -f 05-metabase_postgres.yaml
```
9. Levantamos el dump de la base haciendo algo similar a:
```
kubectl exec -ti metabase-db-0 -- psql -U postgres < metabase/metabase.sql
```
10. Por último levantamos el pod de metabase:
```
kubectl apply -f 06-metabase.yaml
```
11. Para tirar una corrida utilizamos el job:
```
kubectl apply -f runner_job.yaml
```
12. Para activar los cronjobs ejecutar:
```
kubectl apply -f runner_cronjob.yaml
kubectl apply -f updater_cronjob.yaml
```

### Instrucciones para backup y restore
Para habilitar los backups primero debemos crear el pvc para guardarlos con
```
kubectl apply -f backup_pvc.yaml
```
Luego activamos el cronjob
```
kubectl apply -f backup_cronjob.yaml
```
Si queremos hacer un restore deberemos meternos en el pod de timescale. Dentro del mismo hay un volumen /pg_backup con todos los backups. Para realizar el restore seguir las instrucciones en  https://docs.timescale.com/timescaledb/latest/how-to-guides/backup-and-restore/pg-dump-and-restore/#procedure-restoring-an-entire-database-from-backup

## Medidas de seguridad en Kubernetes
1. Al menos los containers de huey_scrapers y huey_prechecks deben correr con usuario no-root, esto se puede realizar como está definido en el manifiesto, es decir, de esta manera: 
   ``` yaml
   securityContext:
           runAsUser: 5000
   ```
2. Es importante que activemos un profile **seccomp** para el container de huey_scraper. Por default, kubernetes no aplica ningún perfil seccomp, por lo tanto los procesos tienen permitido ejecutar cualquier systemcall. Esto significa que por default el scraper funcionará sin problemas pero tendrá más permisos de lo necesario. El perfil a aplicar está definido en el directorio raíz de este proyecto bajo el nombre **chrome.json**. **Recomendamos antes de aplicar este perfil aplicar un perfil extremadamente restrictivo para chequear que se esté aplicando correctamente**. Una vez que esté chequedo, aplicar el **chrome.json**. Para más instrucciones de como implementar los perfiles seccomp en Kubernetes, revisar: https://kubernetes.io/docs/tutorials/clusters/seccomp
3. Similar al punto número 1, es una buena práctica que los containers corran con usuarios no-root, a menos que sea estrictamente necesario correrlos con root. Por lo tanto recomendamos seguir una guía similar a esta para el resto de los containers: https://dzone.com/articles/docker-without-root-privileges. Acompañado a lo anterior es buena práctica también aplicar la siguiente directiva:
   ``` yaml
   securityContext:
      allowPrivilegeEscalation: false
   ```
   Esto última está especificado en: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
4. Aplicar **Network Policies**. Otra medida de seguridad es que cada pod solo acceda o sea accedido en base a lo que necesita y que no esté abierto para todo el cluster. En resumen:
   * huey_scrapers/huey_prechecks: necesitan acceder a timescale y redis. No deben ser accedidos.
   * redis: solo debe ser accedido por huey_scrapers/huey_prechecks. No debe acceder a otros pods
   * timescale-db: solo debe ser accedido por huey_scrapers/huey_prechecks y metabase. No debe acceder a otros pods
   * metabase: no debería ser accedido por otros pods. Solo debe acceder a timescale-db y metabase-db
   * metabase-db: solo debe ser accedido por metabase. No debería acceder a otros pods
   
   Más info en: https://kubernetes.io/docs/concepts/services-networking/network-policies
5. Usuario postgres, tanto en timescale-db como metabase-db se está utilizando el usuario postgres para acceder a la base de datos. Esto no es ideal porque tiene más permisos de lo que debería. Sería una buena medida crear un usuario en postgres que solo conozca la bd que va a utilizar en cada caso y que tenga permisos limitados solamente a leer y escribir, es decir, que no pueda dropear/crear dbs/crer tablas, etc.
